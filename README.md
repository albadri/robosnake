# Robosnake

This is an implementation of a 4-player [Snake](https://en.wikipedia.org/wiki/Snake_(video_game_genre)) game, where each player is controlled by a different piece of AI code.

(The AI code is very, **very** basic! You will be able to understand this codebase even if you've never studied AI.)

You are a developer at an organization that wants to ship this game to users. People will "play" the game by trying to program clever AI agents (or "bots") to compete against other people's AI agents.

This repository currently contains prototype code which works as intended, but the code is not very readable or maintainable. Your job is to refactor the codebase so that it's easier to extend with new features.


## Players and referees

A human player will generally develop an AI agent individually, in their own copy of the codebase. The "Agent capabilities" section below specifies what an AI agent should and should not be able to do.

The four agents in the current version of the code are just example agents for the purpose of testing. They might be removed entirely and replaced with other example agents before the game ships to customers. Most importantly, **these four example agents do not define the limits of what an AI agent should be able to do**.

When multiple human players meet up to compete, they will each originally have their AI agent code in separate copies of the codebase. Each competitive game will have a referee: a human being who takes the players' AI agent code and integrates it into a single copy of the codebase.

The referee should be able to freely assign any player's agent to any snake in the game: for example, one player's agent code might be used to control the green snake in one game, and then the same agent code might be used to control the orange snake in the next game. It should also be possible to use a single player's agent code to control multiple snakes in the same game, without needing to copy the agent code more than once.

**The referee's job should be as easy as possible.** This is one of the primary goals of the codebase.


## Gameplay

There is no direct user input in the game. All four snakes are controlled by AI agent code. There are no plans to add more than four snakes in future versions of the game.

The game board is a square grid of square cells, drawn below the "Statistics" table. The cells on the grid are the "pixels" on the "screen" that displays the game.

There are four snakes, each represented by a letter and a color: A (green), B (blue), C (orange), and D (purple). There are no plans to let players customize the color of the snake that their agent controls.

Each snake starts in a different corner of the game board. On a snake's turn, it is allowed to move one cell up, down, left, or right.

The snakes take turns in order: A moves first, then B, C, D, and then A again. The game screen updates the position of every snake simultaneously, but if two snakes try to move into the same cell, the "tie" is broken by turn order.

When a snake moves into a cell, the snake "takes" that cell. To indicate this, the cell changes to display the color of the snake.

A cell may contain an apple, which is indicated by the color red. In the "Statistics" table, each snake has a counter of how many apples they've taken. After each round of turns, new apples are added to the board in random unoccupied cells.

A snake loses if they try to move into a cell that has already been taken, or if they try to move outside the boundaries of the game board. This includes if a snake tries to move "backwards" (in the opposite direction as its previous move).

The game is over when all snakes have lost. The winning snake is the snake with the highest apples counter at the end of the game, and the winning human player is the developer of the AI agent that was controlling the winning snake.


## Agent capabilities

The AI agents cannot "see" the entire board: on their turn, they can only "see" a 5x5 region of cells centered around their current position. It is considered "cheating" if an agent accesses any any other board data, or any data that should be private to the other snakes. **The design of the codebase should not make it easy for players to write agents that cheat.**

This restriction is implemented in the type of the `agentMove` function, which controls the AI's behavior on each move. This function is **deliberately** not given access to the data of the whole board: it's only given access to the data of the 5x5 region that the current snake should be able to "see".

Except for this restriction, an agent should be able to do **anything that is possible to do in TypeScript**. Each agent should be able to keep its own "private" state and compute its next move in any way.


## The codebase

The codebase is currently organized into multiple modules within the `src` folder:
- `DrawingLibrary.ts` is a library which is used by the rest of the code. You **must not modify** this file: it's owned by a different team of developers.
- `GameScreen.ts` defines the `Cell` and `GameScreen` types, which represent "pixels" on the "game board".
- `GameRunner.ts` defines the rules of the game, but **not** the behavior of the AI agents.
- `Agent.ts` defines the behavior of each AI player.
